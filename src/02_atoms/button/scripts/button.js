import React from 'react';
import '../styles/_button.scss';

/* Default button
---------------------------------------------*/
function Button({
	className = '', 
	children = '', 
	...props
}) {
	
	const theClassName = `button ${className}`;

	return (
		<button type="button" className={theClassName} {...props} >
			{ children }
		</button>
	);
}

export default Button;
