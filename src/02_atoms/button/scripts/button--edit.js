import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_button.scss';
import '../styles/_button--edit.scss';

/* Car edit button
---------------------------------------------*/
function Editbutton(props) {

	const { handleSelectedArea } = useContext(EditorContext);

	return (
		<button
			className="button button--edit"
			type="button"
			onClick={() => handleSelectedArea(props.area)}
		>
			<i className="fa fa-wrench"></i>
		</button>
	);
}

export default Editbutton;
