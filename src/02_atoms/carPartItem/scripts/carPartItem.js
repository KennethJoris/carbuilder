import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_carPartItem.scss';

/* Carpart option
---------------------------------------------*/
function CarPartItem(props) {

	const { handleCarPartsSelection } = useContext(EditorContext);
	const { className, isSelected, handlerData, title, children } = props;

	return (
		<li
			className={`carPartListing__item carPartListing__item--${className} ${isSelected ? 'is-selected' : ''}`}
			onClick={() => handleCarPartsSelection(handlerData)}
		>
			{children}
			<p className="carPartListing__title">{title.toUpperCase()}</p>
		</li>
	);
}

export default CarPartItem;
