import React from 'react';
import '../styles/_window.scss';

/* Carpart option - Door sub-component
---------------------------------------------*/
function Window(props) {
	return (
		<div className={`window ${props.canOpen ? 'window--can-open' : ''}`}></div>
	);
}

export default Window;
