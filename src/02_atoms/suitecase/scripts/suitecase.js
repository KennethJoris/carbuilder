import React from 'react';
import '../styles/_suitecase.scss';

/* Carpart option - Luggage sub-component
---------------------------------------------*/
function Suitecase(props) {

	const { size } = props;

	return (
		<div className={`suitecase ${size ? 'suitecase--' + size : ''}`} ></div >
	);
}

export default Suitecase;
