import React from 'react';
import '../styles/_tire.scss';

/* Carpart option - Wheel sub-component
---------------------------------------------*/
function Tire(props) {
	return (
		<div className={`tire 
			${props.sidewall ? 'tire--sidewall' : ''}
		`}></div>
	);
}

export default Tire;
