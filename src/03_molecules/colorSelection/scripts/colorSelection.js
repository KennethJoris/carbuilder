import React from 'react';
import '../styles/_colorSelection.scss';
import Colorbutton from '02_atoms/button/scripts/button--color';

/* Editor component - Car color choices
---------------------------------------------*/
function ColorSelection() {

	const colorOptions = [
		{
			cssVariable: '--car--color-0',
			buttonClass: 'car--color-0'
		},
		{
			cssVariable: '--car--color-1',
			buttonClass: 'car--color-1'
		},
		{
			cssVariable: '--car--color-2',
			buttonClass: 'car--color-2'
		},
		{
			cssVariable: '--car--color-3',
			buttonClass: 'car--color-3'
		}
	];

	return (
		<div className="colorSelection">
			<span>Car color:</span>
			<ul className="colorSelection__listing">
				{
					colorOptions.map((option, i) => {

						const { cssVariable, buttonClass } = option;

						return (
							<li className="colorSelection__item" key={i}>
								<Colorbutton
									cssVariable={cssVariable}
									buttonClass={buttonClass}
								/>
							</li>
						);
					})
				}
			</ul>
		</div>
	);
}

export default ColorSelection;
