import React from 'react';
import '../styles/_luggage.scss';
import Suitecase from '02_atoms/suitecase/scripts/suitecase';
import RoofRack from '02_atoms/roofRack/scripts/roofRack';

/* Carpart option
---------------------------------------------*/
function Luggage() {
	return (
		<div className="luggage">
			<Suitecase size="large" />
			<Suitecase />
			<Suitecase size="small" />
			<RoofRack />
		</div>
	);
}

export default Luggage;
