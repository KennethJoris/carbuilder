import React from 'react';
import '../styles/_wheel.scss';
import Rim from '02_atoms/rim/scripts/rim';
import Tire from '02_atoms/tire/scripts/tire';

/* Car default component
---------------------------------------------*/
function Wheel(props) {

	const { wheel } = props;
	
	return (
		<div className="wheel">
			<Tire {...wheel} />
			<Rim {...wheel} />
		</div >
	);
}

export default Wheel;
