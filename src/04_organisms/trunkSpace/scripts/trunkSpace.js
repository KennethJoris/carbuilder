import React, { useContext } from 'react';
import { EditorContext } from '00_global/scripts/editorContext';
import '../styles/_trunkSpace.scss';
import Bumper from '02_atoms/bumper/scripts/bumper';
import Wheel from '03_molecules/wheel/scripts/wheel';
import Trunk from '03_molecules/trunk/scripts/trunk';
import Spoiler from '02_atoms/spoiler/scripts/spoiler';
import RoofExtention from '02_atoms/roofExtention/scripts/roofExtention';
import Editbutton from '02_atoms/button/scripts/button--edit';

/* Car section - rear
---------------------------------------------*/
function TrunkSpace() {

	const { components } = useContext(EditorContext);

	return (
		<div className="trunkSpace">
			{
				components.roofExtention && <RoofExtention />
			}
			{
				components.spoiler && <Spoiler />
			}
			<Trunk />
			<Wheel wheel={components.wheel} />
			<Bumper back />
			<Editbutton
				area={'trunkSpace'}
			/>
		</div>
	);
}

export default TrunkSpace;
